# Vagrant lab in order to practice Ansible

## Setup box image from Packer

### Build box image
Create box image from packer to create **user** use for all server in lab
```
cd cluster
packer build packer.vagrant.ansible.json
```
After build we will have folder **output-vagrant** contain file **package.box**

### Create box image name

Add box offine in box list of local to use in Vagrantfile
```
vagrant box add ./output-vagrant/package.box --name duytq/centos7
```
You can replace the flag `--name` ever what you want

## Bring up the Labs
Modify the name of box using in Vagrant according to you create above in line 5
`config.vm.box = "duytq/centos7"`

```
vagrant up
```

## To shutdown the cluster
```
vagrant halt
```

## To restart the cluster
Restart all server you have
```
vagrant up
```
Reload specific server
```
vagrant reload <server-name>
```
if you want reload provision specific using more flag `--provision`
## To destroy the cluster
```
vagrant destroy -f
```
