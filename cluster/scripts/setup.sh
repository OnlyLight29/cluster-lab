#!/bin/bash

useradd ansible
echo "ansible" | passwd --stdin ansible
usermod -aG wheel ansible
sed -i "s/# %wheel/%wheel/" /etc/sudoers

sed -i 's/^PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config
systemctl reload sshd
